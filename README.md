# Extract Admin Boundaries

A C++ program for use with the [Osmium library](https://github.com/osmcode/libosmium) to extract admin boundaries from OSM PBF files.
The extract is written to a OSM PBF file.

## Compiling

Make sure that you have setup your development environment. There are various tutorials out there how to do that.
Install [dependencies](https://github.com/osmcode/libosmium/wiki/Libosmium-dependencies) that are required by Osmium, namely zlib and Boost iterator.
Checkout a copy of Osmium from GitHub and place it within this repository or any other location you want.

Compile with:
``g++ -Ipath/to/libosmium/include -std=c++11 -o extract-admin extract-admin.cpp -pthread -lz``

If you want to include debug symbols add the ``-g`` option.

## Usage

Run the program with ``./extract-admin input.osm.pbf output.osm.pbf`` to extract countries (admin_level=2) or specify individual admin levels with a comma-separated list
e.g. ``./extract-admin 2,4 input.osm.pbf output.osm.pbf`` to get countries and states (admin_level=2, admin_level=4).

## License

Available under the MIT License. See [LICENSE.txt](https://github.com/gmgeo/extract-admin/blob/master/LICENSE.txt) for details.
