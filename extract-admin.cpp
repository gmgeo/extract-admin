#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>
#include <sstream>
#include <vector>

#include <osmium/io/pbf_input.hpp>
#include <osmium/io/pbf_output.hpp>
#include <osmium/io/output_iterator.hpp>
#include <osmium/handler.hpp>
#include <osmium/osm/types.hpp>

class AdminRelationHandler : public osmium::handler::Handler {

  std::unordered_map<long long int, osmium::item_type>& id_store;
  std::vector<std::string>& levels;

public:

  AdminRelationHandler(std::unordered_map<long long int, osmium::item_type>& id_store_, std::vector<std::string>& levels_)
  : id_store(id_store_), levels(levels_) {}

  void relation(const osmium::Relation& rel) {
    std::string boundary(rel.tags()["boundary"] ? rel.tags()["boundary"] : "");
    std::string type(rel.tags()["type"] ? rel.tags()["type"] : "");
    std::string level(rel.tags()["admin_level"] ? rel.tags()["admin_level"] : "");

    if (boundary == std::string("administrative") && type == std::string("boundary") && !level.empty()) {
      for (const std::string confLevel : levels) {
        if (level == confLevel) {
          id_store[rel.id()] = osmium::item_type::relation;

          const osmium::RelationMemberList& rml = rel.members();
          for (const osmium::RelationMember& rm : rml) {
            if (rm.type() == osmium::item_type::way) {
              id_store[rm.ref()] = rm.type();
            }
          }
        }
      }
    }
  }
};

class AdminWayHandler : public osmium::handler::Handler {

  std::unordered_map<long long int, osmium::item_type>& id_store;

public:

  AdminWayHandler(std::unordered_map<long long int, osmium::item_type>& id_store_)
  : id_store(id_store_) {}

  void way(const osmium::Way& way) {
    auto id = id_store.find(way.id());
    if (id != id_store.end() && id->second == osmium::item_type::way) {
      for (const osmium::NodeRef& nr : way.nodes()) {
        id_store[nr.ref()] = osmium::item_type::node;
      }
    }
  }
};

int main(int argc, char* argv[]) {

  if (argc < 3 || argc > 4) {
    std::cerr << "Usage: " << argv[0] << "[1,2,3,4,5,6] input.osm.pbf output.osm.pbf\n";
    std::cerr << "where [1,2,3,4,5,6] specifies an optional list of admin levels.\n";
    exit(1);
  }

  std::vector<std::string> levels;

  if (argc == 4) {
    std::stringstream stream(argv[1]);
    std::string item;
    while (std::getline(stream, item, ',')) {
      levels.push_back(item);
    }
  }
  else {
    levels.push_back(std::string("2"));
  }

  osmium::io::File input_file(argc == 3 ? argv[1] : argv[2]);
  osmium::io::File output_file(argc == 3 ? argv[2] : argv[3]);

  std::unordered_map<long long int, osmium::item_type> id_store;

  std::cerr << "pass 1 - getting relations and their ways\n";

  osmium::io::Reader reader1(input_file, osmium::osm_entity_bits::relation); // we need only relations this time
  AdminRelationHandler admin_rel_handler(id_store, levels);

  osmium::apply(reader1, admin_rel_handler);
  reader1.close();

  std::cerr << "pass 2 - getting nodes of ways\n";

  osmium::io::Reader reader2(input_file, osmium::osm_entity_bits::way); // we need only ways this time
  AdminWayHandler admin_way_handler(id_store);

  osmium::apply(reader2, admin_way_handler);
  reader2.close();

  std::cerr << "pass 3 - writing objects to file\n";

  osmium::io::Reader reader3(input_file, osmium::osm_entity_bits::relation | osmium::osm_entity_bits::way | osmium::osm_entity_bits::node);
  osmium::io::Header header = reader3.header();
  osmium::io::Writer writer(output_file, header, osmium::io::overwrite::allow);

  auto input_begin = osmium::io::InputIterator<osmium::io::Reader, osmium::OSMObject> {reader3};
  auto input_end = osmium::io::InputIterator<osmium::io::Reader, osmium::OSMObject> {};
  auto output_iterator = osmium::io::OutputIterator<osmium::io::Writer> {writer, 100 * 1024};

  std::copy_if(input_begin, input_end, output_iterator, [&id_store](const osmium::OSMObject& obj) {
    auto id = id_store.find(obj.id());
    if (id != id_store.end()) {
      return true;
    }

    return false;
  });

  output_iterator.flush();
  writer.close();
  reader3.close();

  std::cerr << "done.\n";
}
